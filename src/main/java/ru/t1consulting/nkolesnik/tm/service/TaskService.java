package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.service.ITaskService;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.DescriptionEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.IndexIncorrectException;
import ru.t1consulting.nkolesnik.tm.exception.field.NameEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.TaskIdEmptyException;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final ITaskRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        return repository.create(userId, name);
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        @NotNull final Task task = create(userId, name, description);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @NotNull
    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(id).orElseThrow(TaskIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Task task = findById(userId, id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(index).filter(idx -> idx > -1).orElseThrow(IndexIncorrectException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Task task = findByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @NotNull final Status status
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(index).filter(idx -> idx > -1).orElseThrow(IndexIncorrectException::new);
        @NotNull final Task task = findByIndex(userId, index);
        task.setStatus(status);
        return task;
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(@Nullable final String userId, final String id, @NotNull final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(id).orElseThrow(TaskIdEmptyException::new);
        @NotNull final Task task = findById(userId, id);
        task.setStatus(status);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        List<Task> tasks = repository.findAllByProjectId(userId, projectId);
        if (tasks == null || tasks.isEmpty()) return Collections.emptyList();
        return tasks;
    }

}
